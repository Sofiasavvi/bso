import FUNCTIONS
import openpyxl
import bso
from openpyxl import load_workbook
import numpy as np
import os
import sys
import time

os.environ['OMP_NUM_THREADS'] = '1'
# INITIALIZE XL FILE
file_name = "/BSO/kris exp.xlsx"
if os.path.exists(file_name):

    # Load existing workbook
    wb = load_workbook(file_name)
    ws = wb.active
else:
    # Generate a new workbook
    wb = openpyxl.Workbook()
    ws = wb.active
    # Define data to be appended
    data = (
        ('', 'Best value', 'Worst value', 'Mean', "Dx", "Df", "Dt"),
        (" ", " ", " ", " ", " ", " ", " ")
    )
start = time.time()
# EXPERIMENT 1.

# Run all functions with ξ1

# ξ1 = a * rand() * np.exp(1-(T/(T-t+1)))

# EXP(D,N,T,X_max,X_min)

# UNIMODAL

# SPHERE
bso.EXP(2, 20, 500, 100, -100, FUNCTIONS.sphere,
        true_min=0, true_min_x=0, xi=1)
bso.EXP(10, 25, 500, 100, -100, FUNCTIONS.sphere,
        true_min=0, true_min_x=0, xi=1)
bso.EXP(20, 40, 500, 100, -100, FUNCTIONS.sphere,
        true_min=0, true_min_x=0, xi=1)

# levy_function
bso.EXP(2, 20, 500, 10, -10, FUNCTIONS.levy_function,
        true_min=0, true_min_x=1, xi=1)
bso.EXP(10, 25, 500, 10, -10, FUNCTIONS.levy_function,
        true_min=0, true_min_x=1, xi=1)
bso.EXP(20, 40, 500, 10, -10, FUNCTIONS.levy_function,
        true_min=0, true_min_x=1, xi=1)

# Styblinski–Tang function
bso.EXP(2, 20, 500, 5, -5, FUNCTIONS.Styblinski_Tang,
        true_min=-39.166165703771416*2, true_min_x=-2.903534, xi=1)
bso.EXP(10, 25, 500, 5, -5, FUNCTIONS.Styblinski_Tang,
        true_min=-39.166165703771416*10, true_min_x=-2.903534, xi=1)
bso.EXP(20, 40, 500, 5, -5, FUNCTIONS.Styblinski_Tang,
        true_min=-39.166165703771416*20, true_min_x=-2.903534, xi=1)

# Matyas
bso.EXP(2, 20, 500, 5.12, -5.12, FUNCTIONS.Matyas,
        true_min=0, true_min_x=0, xi=1)
bso.EXP(10, 25, 500, 5.12, -5.12, FUNCTIONS.Matyas,
        true_min=0, true_min_x=0, xi=1)
bso.EXP(20, 40, 500, 5.12, -5.12, FUNCTIONS.Matyas,
        true_min=0, true_min_x=0, xi=1)

# EASOM
bso.EXP(2, 20, 500, 100, -100, FUNCTIONS.easom,
        true_min=-1, true_min_x=np.pi, xi=1)


#                                                                 #MULTIMODAL
# ROSENBROCK
bso.EXP(2, 20, 500, 10, -10, FUNCTIONS.rosenbrock,
        true_min=0, true_min_x=1, xi=1)
bso.EXP(10, 25, 500, 10, -10, FUNCTIONS.rosenbrock,
        true_min=0, true_min_x=1, xi=1)
bso.EXP(20, 40, 500, 10, -10, FUNCTIONS.rosenbrock,
        true_min=0, true_min_x=1, xi=1)

# ACKLEY
bso.EXP(2, 20, 500, 32.768, -32.768, FUNCTIONS.ackley,
        true_min=0, true_min_x=0, xi=1)
bso.EXP(10, 25, 500, 32.768, -32.768, FUNCTIONS.ackley,
        true_min=0, true_min_x=0, xi=1)
bso.EXP(20, 40, 500, 32.768, -32.768, FUNCTIONS.ackley,
        true_min=0, true_min_x=0, xi=1)

# RASTRIGIN
bso.EXP(2, 20, 500, 5.12, -5.12, FUNCTIONS.Rastrigin,
        true_min=0, true_min_x=0, xi=1)
bso.EXP(10, 25, 500, 5.12, -5.12, FUNCTIONS.Rastrigin,
        true_min=0, true_min_x=0, xi=1)
bso.EXP(20, 40, 500, 5.12, -5.12, FUNCTIONS.Rastrigin,
        true_min=0, true_min_x=0, xi=1)

# Drop wave
bso.EXP(2, 20, 500, 5.12, -5.12, FUNCTIONS.drop_wave,
        true_min=0, true_min_x=0, xi=1)
bso.EXP(10, 25, 500, 5.12, -5.12, FUNCTIONS.drop_wave,
        true_min=0, true_min_x=0, xi=1)
bso.EXP(20, 40, 500, 5.12, -5.12, FUNCTIONS.drop_wave,
        true_min=0, true_min_x=0, xi=1)

# Schaffer_N2
bso.EXP(2, 20, 500, 100, -100, FUNCTIONS.Schaffer_N2,
        true_min=0, true_min_x=0, xi=1)


# INITIALIZE XL FILE
file_name = "BSO/output_ξ2_k=20.xlsx"


if os.path.exists(file_name):

    # Load existing workbook
    wb = load_workbook(file_name)
    ws = wb.active
else:
    # Generate a new workbook
    wb = openpyxl.Workbook()
    ws = wb.active
    # Define data to be appended
    data = (
        ('', 'Best value', 'Worst value', 'Mean', "Dx", "Df", "Dt"),
        (" ", " ", " ", " ", " ", " ", " ")
    )

# EXPERIMENT 2.

# Run all functions with ξ2

# ξ2 = logarithmic_sigmoid((0.5*T-t)/k)*rand()

# EXP(D,N,T,X_max,X_min)

# UNIMODAL

# SPHERE
bso.EXP(2, 20, 500, 100, -100, FUNCTIONS.sphere,
        true_min=0, true_min_x=0, xi=2)
bso.EXP(10, 25, 500, 100, -100, FUNCTIONS.sphere,
        true_min=0, true_min_x=0, xi=2)
bso.EXP(20, 40, 500, 100, -100, FUNCTIONS.sphere,
        true_min=0, true_min_x=0, xi=2)

# levy_function

bso.EXP(2, 20, 500, 10, -10, FUNCTIONS.levy_function,
        true_min=0, true_min_x=1, xi=2)
bso.EXP(10, 25, 500, 10, -10, FUNCTIONS.levy_function,
        true_min=0, true_min_x=1, xi=2)
bso.EXP(20, 40, 500, 10, -10, FUNCTIONS.levy_function,
        true_min=0, true_min_x=1, xi=2)

# Styblinski–Tang function

bso.EXP(2, 20, 500, 5, -5, FUNCTIONS.Styblinski_Tang,
        true_min=-39.166165703771416*2, true_min_x=-2.903534, xi=2)
bso.EXP(10, 25, 500, 5, -5, FUNCTIONS.Styblinski_Tang,
        true_min=-39.166165703771416*10, true_min_x=-2.903534, xi=2)
bso.EXP(20, 40, 500, 5, -5, FUNCTIONS.Styblinski_Tang,
        true_min=-39.166165703771416*20, true_min_x=-2.903534, xi=2)

# Matyas

bso.EXP(2, 20, 500, 5.12, -5.12, FUNCTIONS.Matyas,
        true_min=0, true_min_x=0, xi=2)
bso.EXP(10, 25, 500, 5.12, -5.12, FUNCTIONS.Matyas,
        true_min=0, true_min_x=0, xi=2)
bso.EXP(20, 40, 500, 5.12, -5.12, FUNCTIONS.Matyas,
        true_min=0, true_min_x=0, xi=2)

# EASOM

bso.EXP(2, 20, 500, 100, -100, FUNCTIONS.easom,
        true_min=-1, true_min_x=np.pi, xi=2)


#                                                                 #MULTIMODAL

#                                                                               #ROSENBROCK
bso.EXP(2, 20, 500, 10, -10, FUNCTIONS.rosenbrock,
        true_min=0, true_min_x=1, xi=2)
bso.EXP(10, 25, 500, 10, -10, FUNCTIONS.rosenbrock,
        true_min=0, true_min_x=1, xi=2)
bso.EXP(20, 40, 500, 10, -10, FUNCTIONS.rosenbrock,
        true_min=0, true_min_x=1, xi=2)

#                                                                               #ACKLEY
bso.EXP(2, 20, 500, 32.768, -32.768, FUNCTIONS.ackley,
        true_min=0, true_min_x=0, xi=2)
bso.EXP(10, 25, 500, 32.768, -32.768, FUNCTIONS.ackley,
        true_min=0, true_min_x=0, xi=2)
bso.EXP(20, 40, 500, 32.768, -32.768, FUNCTIONS.ackley,
        true_min=0, true_min_x=0, xi=2)


#                                                                               #RASTRIGIN

bso.EXP(2, 20, 500, 5.12, -5.12, FUNCTIONS.Rastrigin,
        true_min=0, true_min_x=0, xi=2)
bso.EXP(10, 25, 500, 5.12, -5.12, FUNCTIONS.Rastrigin,
        true_min=0, true_min_x=0, xi=2)
bso.EXP(20, 40, 500, 5.12, -5.12, FUNCTIONS.Rastrigin,
        true_min=0, true_min_x=0, xi=2)

# Drop wave
bso.EXP(2, 20, 500, 5.12, -5.12, FUNCTIONS.drop_wave,
        true_min=0, true_min_x=0, xi=2)
bso.EXP(10, 25, 500, 5.12, -5.12, FUNCTIONS.drop_wave,
        true_min=0, true_min_x=0, xi=2)
bso.EXP(20, 40, 500, 5.12, -5.12, FUNCTIONS.drop_wave,
        true_min=0, true_min_x=0, xi=2)

# Schaffer_N2
bso.EXP(2, 20, 500, 100, -100, FUNCTIONS.Schaffer_N2,
        true_min=0, true_min_x=0, xi=2)
end = time.time()
run_time = end - start

sys.exit()
