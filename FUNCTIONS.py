import numpy as np

# MULTIMODAL

# Rosenbrock


def rosenbrock(a):
    f = 0
    for i in range(len(a) - 1):
        f += 100 * (a[i+1] - a[i]**2)**2 + (1 - a[i])**2
    return f

# Ackley


def ackley(x):
    a = 20
    b = 0.2
    c = 2 * np.pi
    sum1 = 0
    sum2 = 0
    for i in range(len(x)):
        sum1 += np.sum(x[i]**2)
        sum2 += np.sum(np.cos(c * x[i]))
    term1 = -a * np.exp(-b * np.sqrt(1/len(x) * sum1))
    term2 = -np.exp(1/len(x) * sum2)

    f = term1 + term2 + a + np.exp(1)
    return f

# Rastrigin


def Rastrigin(a):
    y = [x ** 2 - 10 * np.cos(2 * np.pi * x) for x in a]
    f = sum(y) + 10*len(a)
    return f

# Levy


def levy_function(a):
    a = np.array(a)
    d = len(a)
    w = 1 + (a - 1) / 4
    term1 = np.sin(np.pi * w[0]) ** 2
    term2 = np.sum((w - 1) ** 2 * (1 + 10 * np.sin(np.pi * w) ** 2))
    term3 = (w[-1] - 1) ** 2 * (1 + np.sin(2 * np.pi * w[-1]) ** 2)
    return term1 + term2 + term3

# Styblinski–Tang function


def Styblinski_Tang(a):
    d = [x ** 4 - 16*x + 5*x for x in a]
    return (sum(d)/2)

# Drop_wave


def drop_wave(a):
    y = [x**2 for x in a]
    f = 1 - (1 + np.cos(12 * np.sqrt(sum(y)))) / (0.5 * sum(y) + 2)
    return f


# UNIMODAL

# Sphere
def sphere(a):
    y = [x**2 for x in a]
    f = sum(y)
    return f


# Easom
def easom(a):
    f = -np.cos(a[0]) * np.cos(a[1]) * \
        np.exp(-((a[0] - np.pi)**2 + (a[1] - np.pi)**2))
    return f

# Matyas


def Matyas(a):
    a = np.array(a)  # Convert list to NumPy array
    return 0.26 * np.sum(a**2) - 0.48 * np.prod(a)


# Schaffer_N2
def Schaffer_N2(a):
    f = 0.5 + (np.sin(a[0]**2-a[1]**2)**2 - 0.5)/(1+0.001*(a[0]**2+a[1]**2))**2
    return f


# Beale
def Beale(a):
    return (1.5 - a[0] + a[0]*a[1]) ** 2 + (2.25 - a[0] + a[0]*a[1]**2)**2 + (2.625 - a[0] + a[0]*a[1]**3)**2


# McCormick
def McCormick(a):
    return (np.sin(a[0]+a[1]) + (a[0]-a[1])**2 - 1.5*a[0] + 2.5*a[1] + 1)
