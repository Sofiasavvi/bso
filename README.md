# Brainstorm Optimization (BSO)

## Overview
This repository contains the implementation of the Brainstorm Optimization (BSO) algorithm along with supporting scripts for conducting experiments and necessary functions. BSO is a metaheuristic optimization algorithm inspired by the brainstorming process.

## Files

1. **bso.py**: This file contains the implementation of the Brainstorm Optimization algorithm. The main steps of the algorithm are outlined in a flowchart image (`flowchartBSO.png`) for easy reference.

2. **EXPS.py**: This script is responsible for conducting experiments using the BSO algorithm. It utilizes the functions defined in `Functions.py` to perform the experiments.

3. **FUNCTIONS.py**: This file contains the necessary functions used in the experiments conducted with the BSO algorithm.

## How to Use
To utilize the Brainstorm Optimization algorithm and conduct experiments:

1. Ensure that you have Python installed on your system.
2. Clone this repository to your local machine.
3. Run the `EXPS.py` script to conduct experiments using the BSO algorithm.

## Results
After running the experiments, a folder named `BSO` will be exported. This folder contains:
- A folder named `plots` containing any plots or visualizations generated during the experiments.
- Excel files containing the results of the experiments.

## Notes
- Make sure to review and customize the parameters and settings in the `EXPS.py` script according to your specific experiment requirements.
- For any inquiries or issues, please feel free to contact [insert your contact information].
