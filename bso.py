# Importing required libraries

from sklearn.exceptions import ConvergenceWarning
import warnings
import time
import sys
from openpyxl import load_workbook
import openpyxl
import math
import random
from sklearn.cluster import KMeans
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
# %matplotlib inline
import os
os.environ['OMP_NUM_THREADS'] = '1'

# Function that returns random number between 0,1


def rand():
    return random.uniform(0, 1)

# Function that returns the logarithmic sigmoid transfer function


def logarithmic_sigmoid(x):
    return (1/(1+np.exp(-x)))


# Function to get the clusters X values as list, each element of list is one cluster which contains the X values
def find_Clusters(X, m, N):

    # defining the kmeans function with initialization as k-means++
    kmeans = KMeans(n_clusters=m, init='k-means++', n_init="auto")

    # fitting the k means algorithm on scaled data
    with warnings.catch_warnings():
        warnings.filterwarnings("ignore", category=ConvergenceWarning)
        kmeans.fit(X)

    # Finding the clusters indexes, this means that for example the X with index=2 belongs in the second cluster
    indexes = kmeans.labels_

    # Making a list including Xs for every cluster. Has m lists, each list represent one cluster with Xs
    Clusters_X = []

    for i in range(m):
        Clusters_X.append([])

    for i in range(N):
        for j in range(m):
            if (indexes[i] == j):
                Clusters_X[j].append(X[i])

    return Clusters_X


# STEP 5a)                                   One-Cluster Idea Generation
def gen_idea_oneCluster(Y, Z, t, D, T, X_max, X_min, xi):  # Y:Clusters_X, Z: Centers_X
    # Step 5a1

    # num_ideas: array including the number of ideas each Cluster has
    # total_length: total ideas in Y
    # probabilities: array including the probability that each array is selected

    # Calculate probabilities based on the length of each sub-array
    num_ideas = [len(sub_array) for sub_array in Y]
    total_length = sum(num_ideas)
    probabilities = [length / total_length for length in num_ideas]

    # Select a cluster based on the calculated probabilities
    selected_cluster_index = random.choices(
        range(len(Y)), weights=probabilities)[0]

    # Steps: 5a2 & 5a3
    if (rand() < P_oneCluster):
        selected = Z[selected_cluster_index]  # Select the cluster center
    else:
        if len(Y[selected_cluster_index]) > 1:  # Check if the cluster has more than one idea
            flag = True
            countRepeat = 0

            while flag == True:
                # Select a random idea of the cluster
                r = random.randint(0, len(Y[selected_cluster_index]) - 1)
                selected = Y[selected_cluster_index][r]

                if selected != Z[selected_cluster_index]:
                    # exit the loop if the selected idea is different from the center
                    flag = False
                else:
                    countRepeat += 1

                    if countRepeat == 15:
                        flag = False
        # If the cluster has only one idea (just the center) then choose the center
        else:
            selected = Z[selected_cluster_index]

    # Generate the new idea
    Norm = np.random.normal(0, 1, D)
    a = 0.25 * (X_max-X_min)
    if xi == 1:
        ξ = a * rand() * np.exp(1-(T/(T-t+1)))
    elif xi == 2:
        ξ = logarithmic_sigmoid((0.5*T-t)/20)*rand()
    X_new = []

    for i in range(D):
        X_new.append(selected[i] + ξ * Norm[i])
    return X_new


# STEP 5b)                            Two-Cluster Idea Generation
def gen_idea_twoCluster(Y, Z, t, m, D, T, X_max, X_min, xi):  # Y:Clusters_X, Z: Centers_X
    # Step 5b1
    Y = [subarray for subarray in Y if subarray]
    # Select two random indices from non-empty arrays
    selected_indices = random.sample(range(len(Y)), k=2)

    # Steps 5b2 & 5B3
    select = []
    if (rand() < P_twoCluster):
        # Get the selected center from their indices
        select = [Z[index] for index in selected_indices]
    else:
        for i in selected_indices:
            if len(Y[i]) > 1:  # Check if the cluster has more than one idea
                flag = True
                countRepeat = 0
                while flag == True:

                    # Select a random idea of the cluster
                    r = random.randint(0, len(Y[i]) - 1)

                    if Y[i][r] != Z[i]:
                        flag = False  # exit the loop if the selected idea is different from the center
                    else:
                        countRepeat += 1
                        if countRepeat == 15:
                            flag = False
                select.append(Y[i][r])
            else:
                # Handle the case where the cluster has only one idea
                select.append(Z[i])

    # Generate the new idea
    Norm = np.random.normal(0, 1, D)
    R = rand()
    a = 0.25 * (X_max-X_min)
    if xi == 1:
        ξ = a * rand() * np.exp(1-(T/(T-t+1)))

    elif xi == 2:
        ξ = logarithmic_sigmoid((0.5*T-t)/20)*rand()

    X_new = []
    for j in range(D):
        X_new.append(R*select[0][j] + (1-R)*select[1][j] + ξ*Norm[j])
    return X_new


P_replace = 0.2
P_generation = 0.8
P_oneCluster = 0.4
P_twoCluster = 0.5


def BSO(D, N, m, T, X_max, X_min, function, tables, tables_X, true_min, true_min_x, xi):

    best_val = []  # list for storing best values
    best_X = []  # list for storing the Xs of best values

    # STEP 1 - Population Initializations

    # initializing X as random vector of N ideas, where each idea has D dimensions and Filling list F with function
    X = []  # list for ideas

    for j in range(N):
        X_i = []
        for i in range(D):
            X_i.append(X_min+random.uniform(0, 1)*(X_max-X_min))
        X.append(X_i)
    m = int(np.floor((N/5)))

    for t in range(0, T):
        # STEP 2 - CLUSTERING IDEAS
        Clusters_X = find_Clusters(X, m, N)
        Clusters_X = [subarray for subarray in Clusters_X if subarray]

# STEP 3 - RANK IDEAS AND FINDING CENTERS

        F = []
        index_of_min = []

        for i in range(len(Clusters_X)):
            F.append([])
            for j in range(len(Clusters_X[i])):
                F[i].append(function(Clusters_X[i][j]))

        Centers = []
        Centers_X = []

        for i in range(len(Clusters_X)):
            if F[i]:  # Check if F[i] is not empty
                min_value = min(F[i])
                Centers.append(min_value)

                # Check if the list is not empty before finding the index
                Clusters_X = [subarray for subarray in Clusters_X if subarray]
                index_of_min.append(F[i].index(min_value))
                Centers_X.append(Clusters_X[i][index_of_min[i]])

# STEP 4 - DISRUPTING CLUSTER CENTER

        if rand() < P_replace:
            # Randomly select a cluster center and replace it with a random generated idea
            num = random.randint(0, len(Clusters_X)-1)

            for i in range(D):
                Centers_X[num][i] = X_min+rand()*(X_max-X_min)
                Clusters_X[num][index_of_min[num]][i] = Centers_X[num][i]

            Centers[num] = function(Centers_X[num])
            F[num][index_of_min[num]] = Centers[num]

# STEP 5 - UPDATING IDEAS

        for i in range(N):
            # the condition m<2 is because if there is for example just 1 cluster, there is no point in doing the 2cluster idea generation
            if rand() < P_generation or len(Clusters_X) < 2:
                # the new idea is created based on one cluster (Step 5a)
                X_new = gen_idea_oneCluster(
                    Clusters_X, Centers_X, t, D, T, X_max, X_min, xi)

                if function(X_new) < function(X[i]):
                    X[i] = X_new

            else:
                # the new idea is created based on two clusters (Step 5b)
                X_new = gen_idea_twoCluster(
                    Clusters_X, Centers_X, t, m, D, T, X_max, X_min, xi)
                if function(X_new) < function(X[i]):
                    X[i] = X_new

        # Computing the values of function(X) in order to record the best value and put it in list best_val
        values = []
        for i in range(N):
            values.append(function(X[i]))

        # Find the index of the minimum value in the array 'values'
        min_index = values.index(min(values))

        # Retrieve the corresponding X[i] using the index
        best_X.append(X[min_index])

        # Store the results in the arrays
        best_val.append(min(values))

    tables.append(best_val)
    tables_X.append(best_X)
    return (tables, tables_X)


# EXPERIMENTS
def EXP(D, N, T, X_max, X_min, func, true_min, true_min_x, xi):

    m = int(math.floor((N/5)))  # Number of clusters
    tables = []  # List of lists. dimension: KxT. Storing the values for the graph and the final result
    tables_X = []

    # Record the start time
    start_time = time.time()

    for k in range(10):
        print("k is: ", k)
        result = BSO(D, N, m, T, X_max, X_min, func, tables,
                     tables_X, true_min, true_min_x, xi)  # Run BSO

    # Record the end time
    end_time = time.time()
    # Calculate the elapsed time
    run_time = end_time - start_time

    # Computing every value for the graph : the mean of the Tth value for every k in tables
    values_for_graph = []
    for i in range(T):
        sum = 0
        for j in result[0]:
            sum = sum + j[i]
        values_for_graph.append(sum/len(result[0]))

    # PLOT
    plt.figure()
    plot = plt.scatter(list(range(len(values_for_graph))), values_for_graph)

    # Specify the directory for the plot
    if xi == 1:
        save_directory = f'BSO/plots/ξ{xi}_a=0.25'
    elif xi == 2:
        save_directory = f'BSO/plots/ξ{xi}_k=20'

    # Create the directory if it doesn't exist
    if not os.path.exists(save_directory):
        os.makedirs(save_directory)

    # Save the plot to a file
    plt.savefig(f'{save_directory}/{func.__name__}_{D}D_ξ{xi}.png')

    # list for storing the last value of every k. (YiT, where i is from 1 to k)
    list_of_best = []
    list_of_best_X = []
    for k in range(len(result[0])):
        list_of_best.append(result[0][k][len(result[0][k])-1])
        list_of_best_X.append(result[1][k][len(result[1][k])-1])

    # Find the index of the minimum value in the array list of best
    min_index = list_of_best.index(min(list_of_best))

    # Retrieve the corresponding best of best for the Xs using the index
    best_of_best_X = (list_of_best_X[min_index])

    best_of_best = min(list_of_best)
    worst_of_best = max(list_of_best)
    mean = np.mean(list_of_best)
    median = np.median(list_of_best)

    # COMPUTING THE NORMALIZED METRICS
    sum = 0

    for i in range(D):
        sum += ((best_of_best_X[i]-true_min_x)/(X_max-X_min))**2
    Dx = np.sqrt((1/D)*sum)

    # INITIALIZE XL FILE

    if xi == 1:
        file_name = "output_ξ1_a=0.25.xlsx"
    elif xi == 2:
        file_name = "output_ξ2_k=20.xlsx"

    file_path = f'BSO/{file_name}'
    if os.path.exists(file_path):

        # Load existing workbook
        wb = load_workbook(file_path)
        ws = wb.active

        # Append new data
        ws.append((f'{func.__name__}{D}D_ξ{xi}', best_of_best,
                  worst_of_best, mean, median, Dx, run_time))

        # Save the workbook
        wb.save(file_path)
    else:
        # Generate a new workbook
        wb = openpyxl.Workbook()
        ws = wb.active
        # Define data to be appended
        data = (
            ('', 'Best value', 'Worst value', 'Mean', "Median", "Dx", "time"),
            (" ", " ", " ", " ", " ", " ", " ")
        )
        # Append data to Worksheet
        for i in data:
            ws.append(i)

        # Append new data
        ws.append((f'{func.__name__}{D}D_ξ{xi}', best_of_best,
                  worst_of_best, mean, median, Dx, run_time))

        # Save the workbook
        wb.save(file_path)
